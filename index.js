class Person {
    constructor(name, age, salary, sex) {
      this.name = name;
      this.age = age;
      this.salary = salary;
      this.sex = sex;
    }
  
    static quicksort(personArray, field, order) {
      let tempArray = [...personArray];
      if (tempArray.length <= 1) {
        return tempArray;
      }
  
      let pivot = tempArray[tempArray.length - 1][field];
      let pivotItem = tempArray[tempArray.length - 1];
  
      let leftArray = [];
      let rightArray = [];
  
      for (let ele of tempArray.slice(0, tempArray.length - 1)) {
        if (order === "asc") {
          ele[field] < pivot ? leftArray.push(ele) : rightArray.push(ele);
        } else {
          ele[field] < pivot ? rightArray.push(ele) : leftArray.push(ele);
        }
      }
  
      if (leftArray.length > 0 && rightArray.length > 0) {
        return [
          ...this.quicksort(leftArray, field, order),
          pivotItem,
          ...this.quicksort(rightArray, field, order)
        ];
      } else if (leftArray.length > 0) {
        return [...this.quicksort(leftArray, field, order), pivotItem];
      } else {
        return [pivotItem, ...this.quicksort(rightArray, field, order)];
      }
    }
  }
  
  let person1 = new Person("Ankit", 13, 25000, "M");
  let person2 = new Person("Sitesh", 21, 42000, "M");
  let person3 = new Person("Bittu", 13, 39000, "M");
  let person4 = new Person("Shubham", 42, 12000, "M");
  let person5 = new Person("Tony", 35, 55000, "M");
  const personArray = [person1, person2, person3, person4, person5];
  
  console.log(Person.quicksort(personArray, "age", "desc"));
  